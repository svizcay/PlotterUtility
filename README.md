# Plotter Utility

Small utility app that receives data from a socket and plots it using matplotlib.

## How to build plotter.exe
- install pyinstaller (pip install pyinstaller)
- pyinstaller --onefile plotter.py

plotter.exe will be located in the dist folder.


