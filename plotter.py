# globla variables
ip_address = None
port_number = None

# let's have marker colors per track
# marker_colors = {
#         'P' : 'g', # positive marker = green
#         's' : 'r', # negative marker = red
#         '*' : 'y', # question tag marker = yellow
#         }
marker_colors_per_track = [
        {
            'P' : 'r', # positive marker = red
            's' : '#ff6600', # negative marker = orange
            '*' : 'y', # question tag marker = yellow
        },
        {
            'P' : 'g', # positive marker = green
            's' : '#2cba00', # negative marker = light green
            '*' : 'y', # question tag marker = yellow
        },
        ]
marker_sizes = {
        'P' : 14, # positive marker = red
        's' : 10, # negative marker = green
        '*' : 16, # question tag marker = yellow
        }
reversal_marker_sizes = (20, 16)

#plot_needs_redrawing = False

# if false, we use trial id PER track
use_global_trial_id = True

global_trial_id = 1

# lazy import
import threading
lock = threading.Lock()

class Track():

    def __init__(self, color='b', enabled=True):
        self.x_vals = []
        self.y_vals = []
        self.markers = []
        self.reversals = [] # list of indices in the x_vals array (not trial ids)
        self.color = color
        self.enabled = enabled

    def proposed_point(self, value):
        global global_trial_id
        x_value = global_trial_id if use_global_trial_id else (len(self.x_vals)+1)
        self.x_vals.append(x_value)
        self.y_vals.append(value)
        self.markers.append('*')
        #self.notify_plot_changes()
        global_trial_id = global_trial_id + 1

    def evaluate_point(self, evaluation):
        marker = 'P' if evaluation else 's'
        # replace the last marker proposing a value for the actual evaluation
        self.markers[len(self.x_vals)-1] = marker
        #self.notify_plot_changes()

    def notify_reversal(self):
        #index = global_trial_id if use_global_trial_id else (len(self.x_vals)+1)
        # TODO
        # i need to think what is the right index when using global trial id
        # we need to store indices within the x_vals array that contains the reversals
        index = len(self.x_vals) - 1
        self.reversals.append(index)
        #self.notify_plot_changes()

    # def notify_plot_changes(self):
    #     global plot_needs_redrawing
    #     with lock:
    #         plot_needs_redrawing = True

tracks = [Track(color='b'), Track(color='c', enabled=False)]

def create_plot():
    # lazy import (to delay loading matplotlib)
    import matplotlib.pyplot as plt
    from matplotlib.animation import FuncAnimation
    from matplotlib.ticker import MaxNLocator

    def animate(frame_id):

        # global plot_needs_redrawing
        # if (not plot_needs_redrawing):
        #     return

        # is this the same as ax.clear()? having ax the return value from fig.add_subplot()
        plt.cla() # clear the axis, so it plots a single line (instead of having a new one being rendered on top with a different color)

        # to decide whether we force the display of x-axis [0-10]
        less_than_10_datapoints = True if global_trial_id < 10 else False

        # to decide whether we force the display of y-axis [0-1]
        no_data_yet = True

        # line plots
        for track in tracks:
            if track.enabled:
                plt.plot(track.x_vals, track.y_vals, color=track.color, alpha=0.5)
                # if (len(track.x_vals) > 9):
                #     less_than_10_datapoints = False
                if len(track.x_vals) > 0:
                    no_data_yet = False

        # markers
        markers_transparency = 1 if use_global_trial_id else 0.5
        ax = plt.gca()  # Get the current axis
        for track_id, track in enumerate(tracks):
            if track.enabled:
                for i, marker in enumerate(track.markers):
                    plt.plot(
                            track.x_vals[i],
                            track.y_vals[i],
                            marker=marker,
                            markersize=marker_sizes[marker],
                            color=marker_colors_per_track[track_id][marker],
                            alpha=markers_transparency)
                # for reversals:
                for i in track.reversals:
                    plt.plot(
                            track.x_vals[i],
                            track.y_vals[i],
                            marker='o',
                            markersize=reversal_marker_sizes[track_id],
                            fillstyle='none',
                            color=track.color)


        ax.xaxis.set_major_locator(MaxNLocator(integer=True))
        #ax.set_aspect('equal') # this makes the axis equally spaced (which is not good)

        # i think this needs to be set up every time
        x_label = "trial" if use_global_trial_id else "trial per track"

        plt.xlabel(x_label)
        plt.title('Staircase Procedure')
        if (less_than_10_datapoints):
            plt.xlim([0, 10])
        if (no_data_yet):
            plt.ylim([0, 1])
        plt.tight_layout()
        # with lock:
        #     plot_needs_redrawing = False

    plt.style.use('fivethirtyeight')

    #animation = FuncAnimation(plt.gcf(), animate, interval=100) # interval: how fast to to call animation function in milliseconds
    #animation = FuncAnimation(plt.gcf(), functools.partial(animate, art='asdf', y='foo'), interval=100)
    # is this a blocking call? -> No, this is not a blocking call
    #animation = FuncAnimation(plt.gcf(), functools.partial(animate), interval=100)

    x_label = "trial" if use_global_trial_id else "trial per track"
    plt.xlabel(x_label)
    plt.title('Staircase Procedure')
    plt.xlim([0, 10])
    plt.ylim([0, 1])
    plt.tight_layout()

    # Configure x-axis ticks to display only integers
    ax = plt.gca()  # Get the current axis
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))

    animation = FuncAnimation(plt.gcf(), animate, interval=100) # interval: how fast to to call animation function in milliseconds


    thread = threading.Thread(target=socket_communication_thread)
    thread.start()

    # plt.show is a blocking call
    plt.show()

    # make sure the thread finished its execution
    thread.join()

def socket_communication_thread():

    # Server information
    server_ip = ip_address  # Replace with the server's IP address # 127.0.0.1
    server_port = port_number      # Replace with the server's port # 65000
    print(f"Connecting to PTVR app at {server_ip}:{server_port}.")

    # lazy import of the socket module
    import socket

    # Create a socket object
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    connected = False
    try:
        # Connect to the server
        client_socket.connect((server_ip, server_port))
        print("Connection established.")
        connected = True

    except Exception as e:
        print("Error:", str(e))

    if (connected):
        should_run = True
        print("Waiting for incoming datapoints ...")
        while should_run:

            # exit on esc
            # if keyboard.is_pressed("Esc"):
            #     should_run = False

            response = client_socket.recv(1024).decode()
            print(" * Message received: ", response)
            if (response == "quit"):
                should_run = False
            else:
                tokens = response.split(';')
                data_point_type = tokens[0]
                # c# sends the track id as 0 or 1
                # here in python, we also expect them to be 0 and 1
                # for direct access into the array
                track_id = int(tokens[1])
                if (data_point_type == "proposed_point"):
                    value = float(tokens[2])
                    tracks[track_id].proposed_point(value)
                elif (data_point_type == "evaluated_point"):
                    evaluation = False if tokens[2] == "False" else True
                    tracks[track_id].evaluate_point(evaluation)
                elif (data_point_type == "reversal"):
                    tracks[track_id].notify_reversal()
                elif (data_point_type == "enable"):
                    tracks[track_id].enabled = True
                else:
                    print(f"unknown message type {data_point_type}")

        print("Closing connection.")
        client_socket.close()
        print("Connection closed.")


def main():
    print("Plotter Utility")
    import sys
    if len(sys.argv) != 4:
        print("Usage: plotter.exe <IP address> <port number> <use global trial id?>")
        return

    global ip_address
    global port_number
    global use_global_trial_id
    ip_address = sys.argv[1]
    port_number = int(sys.argv[2])
    use_global_trial_id = False if sys.argv[3] == "False" else True

    create_plot()

if __name__ == '__main__':
    main()
